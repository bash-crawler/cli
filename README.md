# Bash Image Crawler CLI


## About
This cli serves quite specific case of website structure: folder-like with .htm/.html pages at the end.

Crawler will go through the links, up to 3 levels of nesting.

Several conditions are applied:
* Only same domain links will be followed.
* Only pages ending with .htm/.html extentions will be parsed for images.

Final list of unique urls to images will be saved to the file `img-urls.txt`.

Or to `img-urls-suffix.txt` if suffix value **suffix** was provided.

**Importnant notice:**
For repeated run - don't forget to remove corresponding .txt files or whole data folder. Or `crawl.sh reset` for whole folder. Otherwise some steps will be skipped.

## Usage
Miminum website url is required as argument.

To get output for usage: `bash ./crawl.sh -h`:

```bash

--------------------------------------------------------------------------------
Bash Crawler CLI version 0.1.1
--------------------------------------------------------------------------------
Usage: crawl.sh http[s]://example.com [suffix] [filter]
  Available commands:
      reset  - to reset data folder(delete all data there), default folder is ./data
  Or corresponding arguments are:
      site   - starting url to crawl, for example https://example.com
      suffix - suffix for output folder ./data
      filter - filter for extracted urls
  Options:
      -v, --version - version output
      -h, --help    - help/usage output for this text
--------------------------------------------------------------------------------
```

To get output for usage: `bash ./crawl.sh -h`:
```bash

--------------------------------------------------------------------------------
Bash Crawler CLI version 0.1.1
--------------------------------------------------------------------------------
```

### Usage examples

`bash crawl.sh http://example.com`
`bash crawl.sh http://example.com [suffix] [filter]`
`bash crawl.sh reset`
`bash crawl.sh -h`
`bash crawl.sh --version`

## How to use resulting list of urls with aria2c

I recommend to use nice tool with support of parallel requests and many other features:

To start download with 4 parallel threads and output to `./images/` folder:
```bash
aria2c -i ./img-urls.txt -j4 -d ./images/
```

See [https://aria2.github.io/](https://aria2.github.io/)

### Contacts
Feel free to contact me with:
 * email/gtalk - [alex@webz.asia](mailto:alex@webz.asia)
 * email - [info@bash-crawler.tk](mailto:info@bash-crawler.tk)

## License

Copyright 2019 Alex aka mailoman <alex@webz.asia>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

#!/usr/bin/env bash

# Copyright 2019 Alex aka mailoman <alex@webz.asia>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# bash web crawler
# $ bash crawl.sh http://example.com
# $ bash crawl.sh http://example.com [suffix] [filter]
# $ bash crawl.sh reset
__version="0.1.1"
__separator="--------------------------------------------------------------------------------"
__default_data_dir="./data"
__default_datetime_format="%Y-%m-%d:%H:%M:%S"
__default_images_ext="\.jpg$|\.png$|\.ico$|\.tiff$|\.tif$|\.gif$"
__default_skipped_ext="\.asp$|\.php$|\.rss$|\.xml$|\.xslt$|\.php$|\.css$|\.js$|\.mov$|\.mp3$|\.mp4$|\.wav$|\.woff$|\.ttf$|\.jpg$|\.png$|\.ico$|\.tiff$|\.tif$|\.gif$"
__default_skipped_urls=(
    "/js/"
    "/css/"
    "/search/"
    "/js?"
    "/css?"
    "/search?"
    "&amp;"
    "&quot;"
    ">"
    "<"
    "&lt;"
    "&gt;"
    "&le;"
    "&ge;"
)
__useragent_chrome="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36"
__useragent_firefox="Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:69.0) Gecko/20100101 Firefox/69.0"
__useragents=(
    "$__useragent_chrome"
    "$__useragent_firefox"
)
__skipping_urls_disable_tag="[[DISABLED_SKIPPING_URLS]]"
# Seed random generator
__RANDOM=$$$(date +%s)

# internal var: prepare excluded urls
skipping_urls=

function exclude_skipped_urls() {
    if [[ -z $skipping_urls ]]; then
        for i in "${__default_skipped_urls[@]}"; do
            delim="|"
            if [[ -z $skipping_urls ]]; then
                delim=""
            fi
            skipping_urls="${skipping_urls}${delim}${i}"
        done
        if [[ -z $skipping_urls ]]; then
            skipping_urls=${__skipping_urls_disable_tag}
        fi
    fi

    if [[ ${skipping_urls} == ${__skipping_urls_disable_tag} ]]; then
        cat
    else
        egrep -v ${skipping_urls}
    fi
}

if [[ $1 == "-v" || $1 == "--version" ]]; then
    echo ${__separator}
    echo "Bash Crawler CLI version ${__version}"
    echo ${__separator}
    exit 0
fi

if [[ $1 == "-h" || $1 == "--help" ]]; then
    echo ${__separator}
    echo "Bash Crawler CLI version ${__version}"
    echo ${__separator}
    echo "Usage: crawl.sh http[s]://example.com [suffix] [filter]"
    echo "  Available commands:"
    echo "      reset  - to reset data folder(delete all data there), default folder is ./data"
    echo "  Or corresponding arguments are:"
    echo "      site   - starting url to crawl, for example https://example.com"
    echo "      suffix - suffix for output folder ./data"
    echo "      filter - filter for extracted urls"
    echo "  Options:"
    echo "      -v, --version - version output"
    echo "      -h, --help    - help/usage output for this text"
    echo ${__separator}
    exit 0
fi

# input data
site=$1
suffix=$2
filter=$3

# internal vars
domain=$(echo $site | cut -d'/' -f3)
proto=$(echo $site | cut -d'/' -f1)
baseurl=${proto}//${domain}/

data_dir=${__default_data_dir}
if [[ ! -z $suffix ]]; then
    data_dir=${__default_data_dir}${suffix}
fi

function timed_label() {
    dt=$(date "+$__default_datetime_format")
    echo $2 "[$dt] $1"
}

if [[ $1 == "reset" ]]; then
    echo ${__separator}
    timed_label "Resetting data folder ${data_dir}"
    rm -rf ${data_dir}
    echo ${__separator}
    exit 0
fi

if [[ -z $site || ! $proto =~ "http" || $site == $proto ]]; then
    echo "Please provide proper site url for crawling! (starting with http or https)"
    exit 1
fi


timed_label "bash web crawler v${__version} - STARTED"

mkdir -p ${data_dir}

function get_request(){
    url=$1
    useragent=${__useragents[$__RANDOM % ${#__RANDOM[*]}]}
    echo $(curl -silent -A "$useragent" "$url")
}

timed_label "starting with base url: $baseurl"

function visit(){
    url=$1
    to=$2
    if [[ $url =~ ^.*\.(htm|html) ]]; then
        timed_label "parsing for img $url"
        if [[ -z $filter ]]; then
            get_request $url | grep "img src=\"" | grep -o "$proto\/\/[^\"]*" | grep $baseurl | egrep ${__default_images_ext} | exclude_skipped_urls >> ${data_dir}/img-urls$suffix.txt
        else
            get_request $url | grep "img src=\"" | grep -o "$proto\/\/[^\"]*" | grep $baseurl | egrep ${__default_images_ext} | exclude_skipped_urls | grep ${filter} >> ${data_dir}/img-urls$suffix.txt
        fi
    elif [[ $url =~ ^.*\.(jpg|jpeg|gif|tiff|tif|png|ico) ]]; then
        timed_label "found img $url"
        echo $url | egrep ${__default_images_ext} | grep $filter >> ${data_dir}/img-urls$suffix.txt
    elif [[ $url =~ ^.*\.(css|js) ]]; then
        timed_label "skipped $url"
    else
        timed_label "visiting $url"
        if [[ -z $filter ]]; then
	        get_request $url | grep href=\" | grep -o "$proto\/\/[^\"]*" | grep $baseurl | egrep -v ${__default_skipped_ext} | exclude_skipped_urls >> $to
        else
	        get_request $url | grep href=\" | grep -o "$proto\/\/[^\"]*" | grep $baseurl | egrep -v ${__default_skipped_ext} | exclude_skipped_urls | grep $filter >> $to
        fi
    fi
}

function backup(){
    filename=$1
#    ext=$2
    ext=.bak
    bakname=${filename}${ext}
    timed_label "backuping $filename => " -n
    i=0
    while $(test -f $bakname)
    do
        ((i++))
        bakname=${filename}.${i}${ext}
    done
    echo $bakname
    mv $filename $bakname
}

function save_list_all(){
    timed_label "updating ${data_dir}/urls$suffix.all.txt"
    from=$1
    cat $from >> ${data_dir}/urls$suffix.all.txt
    sort -o ${data_dir}/urls$suffix.all.sort.txt ${data_dir}/urls$suffix.all.txt
    uniq ${data_dir}/urls$suffix.all.sort.txt ${data_dir}/urls$suffix.all.uniq.txt
    cp ${data_dir}/urls$suffix.all.uniq.txt ${data_dir}/urls$suffix.all.txt
}

## crawl level 0
timed_label "zero level"
if [[ ! -f ${data_dir}/urls$suffix.txt ]]; then
    visit $site ${data_dir}/urls$suffix.txt
fi

if [[ $(cat ${data_dir}/urls$suffix.txt | wc -l | grep -Eo [0-9]+) == "0" ]]; then
    timed_label "nothing found at first request!"
    exit
fi

## sort/uniq
timed_label "zero - sort"
if [[ ! -f ${data_dir}/urls$suffix.sort.txt ]]; then
    sort -o ${data_dir}/urls$suffix.sort.txt ${data_dir}/urls$suffix.txt
    uniq ${data_dir}/urls$suffix.sort.txt ${data_dir}/urls$suffix.uniq.txt
    backup ${data_dir}/urls$suffix.txt
    cp ${data_dir}/urls$suffix.uniq.txt ${data_dir}/urls$suffix.txt
fi

## reset full list
touch ${data_dir}/urls$suffix.all.txt
## save full list - initial, from level 0
timed_label "zero - init all"
save_list_all ${data_dir}/urls$suffix.txt

## crawl level 1
timed_label "level 1"
if [[ -f ${data_dir}/urls$suffix.txt && ! -f ${data_dir}/sub-1-urls$suffix.txt ]]; then
    while read line
    do
        visit $line ${data_dir}/sub-1-urls$suffix.txt $filter
    done < ${data_dir}/urls$suffix.txt
fi

## sort/uniq
timed_label "level 1 - sort"
if [[ ! -f ${data_dir}/sub-1-urls$suffix.sort.txt ]]; then
    sort -o ${data_dir}/sub-1-urls$suffix.sort.txt ${data_dir}/sub-1-urls$suffix.txt
    uniq ${data_dir}/sub-1-urls$suffix.sort.txt ${data_dir}/sub-1-urls$suffix.uniq.txt
    backup ${data_dir}/sub-1-urls$suffix.txt
    cp ${data_dir}/sub-1-urls$suffix.uniq.txt ${data_dir}/sub-1-urls$suffix.txt
fi

### substract ${data_dir}/urls$suffix.all.txt from ${data_dir}/sub-1-urls$suffix.txt
timed_label "level 1 - no duplicates"
if [[ ! -f ${data_dir}/sub-1-urls$suffix.nodup.txt ]]; then
    cp ${data_dir}/sub-1-urls$suffix.txt ${data_dir}/sub-1-urls$suffix.nodup.txt
    while read line
    do
        sed -i '' 's!^'$line'$!!g' ${data_dir}/sub-1-urls$suffix.nodup.txt
    done < ${data_dir}/urls$suffix.all.txt
    sed -i '' '/^$/d' ${data_dir}/sub-1-urls$suffix.nodup.txt
    backup ${data_dir}/sub-1-urls$suffix.txt
    cp ${data_dir}/sub-1-urls$suffix.nodup.txt ${data_dir}/sub-1-urls$suffix.txt
fi

## save full list with previous urls
timed_label "level 1 - save all"
save_list_all ${data_dir}/sub-1-urls$suffix.txt

## crawl level 2
timed_label "level 2"
if [[ -f ${data_dir}/sub-1-urls$suffix.txt && ! -f ${data_dir}/sub-2-urls$suffix.txt ]]; then
    while read line
    do
        visit $line ${data_dir}/sub-2-urls$suffix.txt $filter
    done < ${data_dir}/sub-1-urls$suffix.txt
fi

## sort/uniq
timed_label "level 2 - sort"
if [[ ! -f ${data_dir}/sub-2-urls$suffix.sort.txt ]]; then
    sort -o ${data_dir}/sub-2-urls$suffix.sort.txt ${data_dir}/sub-2-urls$suffix.txt
    uniq ${data_dir}/sub-2-urls$suffix.sort.txt ${data_dir}/sub-2-urls$suffix.uniq.txt
    backup ${data_dir}/sub-2-urls$suffix.txt
    cp ${data_dir}/sub-2-urls$suffix.uniq.txt ${data_dir}/sub-2-urls$suffix.txt
fi

### substract ${data_dir}/urls$suffix.all.txt from ${data_dir}/sub-2-urls$suffix.txt
timed_label "level 2 - no duplicates"
if [[ ! -f ${data_dir}/sub-2-urls$suffix.nodup.txt ]]; then
    cp ${data_dir}/sub-2-urls$suffix.txt ${data_dir}/sub-2-urls$suffix.nodup.txt
    while read line
    do
        sed -i '' 's!^'$line'$!!g' ${data_dir}/sub-2-urls$suffix.nodup.txt
    done < ${data_dir}/urls$suffix.all.txt
    sed -i '' '/^$/d' ${data_dir}/sub-2-urls$suffix.nodup.txt
    backup ${data_dir}/sub-2-urls$suffix.txt
    cp ${data_dir}/sub-2-urls$suffix.nodup.txt ${data_dir}/sub-2-urls$suffix.txt
fi

## save full list with previous urls
timed_label "level 2 - save all"
save_list_all ${data_dir}/sub-2-urls$suffix.txt

## crawl level 3
### visit
timed_label "level 3"
if [[ -f ${data_dir}/sub-2-urls$suffix.txt && ! -f ${data_dir}/sub-3-urls$suffix.txt ]]; then
    while read line
    do
        visit $line ${data_dir}/sub-3-urls$suffix.txt $filter
    done < ${data_dir}/sub-2-urls$suffix.txt
fi

### sort/uniq
timed_label "level 3 - sort"
if [[ ! -f ${data_dir}/sub-3-urls$suffix.sort.txt ]]; then
    sort -o ${data_dir}/sub-3-urls$suffix.sort.txt ${data_dir}/sub-3-urls$suffix.txt
    uniq ${data_dir}/sub-3-urls$suffix.sort.txt ${data_dir}/sub-3-urls$suffix.uniq.txt
    backup ${data_dir}/sub-3-urls$suffix.txt
    cp ${data_dir}/sub-3-urls$suffix.uniq.txt ${data_dir}/sub-3-urls$suffix.txt
fi

### substract ${data_dir}/urls$suffix.all.txt from ${data_dir}/sub-3-urls$suffix.txt
timed_label "level 3 - no duplicates"
if [[ ! -f ${data_dir}/sub-3-urls$suffix.nodup.txt ]]; then
    cp ${data_dir}/sub-3-urls$suffix.txt ${data_dir}/sub-3-urls$suffix.nodup.txt
    while read line
    do
        sed -i '' 's!^'$line'$!!g' ${data_dir}/sub-3-urls$suffix.nodup.txt
    done < ${data_dir}/urls$suffix.all.txt
    sed -i '' '/^$/d' ${data_dir}/sub-3-urls$suffix.nodup.txt
    backup ${data_dir}/sub-3-urls$suffix.txt
    cp ${data_dir}/sub-3-urls$suffix.nodup.txt ${data_dir}/sub-3-urls$suffix.txt
fi

## final sort/uniq img list
timed_label "final img sort"
if [[ ! -f ${data_dir}/img-urls$suffix.sort.txt ]]; then
    sort -o ${data_dir}/img-urls$suffix.sort.txt ${data_dir}/img-urls$suffix.txt
    uniq ${data_dir}/img-urls$suffix.sort.txt ${data_dir}/img-urls$suffix.uniq.txt
    backup ${data_dir}/img-urls$suffix.txt
    cp ${data_dir}/img-urls$suffix.uniq.txt ${data_dir}/img-urls$suffix.txt
fi

total_imgs=$(cat ${data_dir}/img-urls${suffix}.txt | wc -l)
timed_label "total image urls collected: $total_imgs"

timed_label "DONE"
